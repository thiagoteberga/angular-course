import { Component } from '@angular/core';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent{
  public url:string = "/sobre";

  exibeAlerta(evento){
    console.log(evento);
    alert("OK");
  }

  public produtos = [
    {titulo : 'Protuto1', descricao: 'Desc. Produto 1'},
    {titulo : 'Protuto2', descricao: 'Desc. Produto 3'},
    {titulo : 'Protuto3', descricao: 'Desc. Produto 3'},
    {titulo : 'Protuto4', descricao: 'Desc. Produto 4'}
  ];

  public nome: string = "Guilherme";
}
